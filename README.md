SpliTee
=======
Splitting tee - reads data from standard input and writes to standard output
and into file(s) with the ability to split the data into multiple files over
time.
 - `stdin -> stee -> stdout` - works without interruption (like tee)
 - `stdin -> stee -> file(s)`
   - _split interval_ can be provided - a new output file will be created every
     `<interval>` seconds
   - can be controlled using a named pipe
   - output file name is controlled by the OUTPUT FILE PATTERN

FIFO CONTROL (named pipe)
-------------------------
If the `--ctl <ctl path pattern>` argument is passed to stee, it will crate a
named pipe which can be used to controll stee's output to file(s) (`<ctl
path pattern>` is parsed the same as OUTPUT FILE PATTERN).

The fifo accepts the following controll characters

| char | action                                                            |
|------|-------------------------------------------------------------------|
| `s`  | stop writing (closes output file, _resume_ will create new file)  |
| `p`  | pause writing (_resume_ will resume writing into the same file)   |
| `q`  | quit - terminaltes program                                        |
| `n`  | create a new output file                                          |
| `r`  | resume/start writing to file                                      |

All other characters are ignored.

OUTPUT FILE PATTERN
-------------------
Output file name is specified using a pattern `--file <pattern>` and accepts
the following formating characters

| formatting sequence | output                           |
|---------------------|----------------------------------|
| `%n`                | file number (printed using %02d) |
| `%p`                | process id                       |
| `%u`                | user id                          |
| `%T`                | time (HH:MM:SS)                  |
| `%D`                | ISO 8601 date (year-month-day)   |
| `%H`                | hour                             |
| `%M`                | minute                           |
| `%S`                | second                           |
| `%s`                | unix timestamp                   |
| `%%`                | %                                |

All other characters are used unchanged. WARNING: this includes `/`, meaning
formatting characters can be used in directory names as well, **but** the
program assumes that all the directories already exist. If you use a
formatting character in a directory name, make sure that the directories exist.

USAGE
-----
```
stee --file <filename> [--no-write] [--split <interval>] [--ctl <ctl_pattern>]
   [--log <logfile>] [--loglevel <loglevel>] [--verbose] [--quiet] [--help]
   [--output ( none | - | exec <prog> [<arg1> [<arg2> ...] ] ) ]

stee [...] <filename>

   <filename> formating sequences: %n (file number), %T (time HH:MM:SS),
      %D (date YYYY-MM-DD), %H (hour), %M (minute), %S (second),
      %p (PID), %u (UID), %s (unix timestamp), %% (%)
   ctl command characters: 'n' (create new file), 'r' (start recording),
      's' (stop recording), 'p' (pause recording), 'q' (quit)

    --file <filename>
        specifies output file pattern

    --no-write, -n
        don't immediately write to file, wait for fifo ctl 'r' command

    --split, -s <interval>
        when writing to file, creating new file every <interval> seconds

    --ctl, -c <ctl_pattern>
        create fifo ctl at <ctl_pattern> (uses same formatting characters as
        output <filename>

    --log, -l <logfile>
        log to <logfile> instead of stderr

    --loglevel, -L <loglevel>
        one of quiet, error, info, debug (defaults to info)

    --quiet, -q
        sets loglevel to quiet

    --verbose, -v
        increases loglevel (positionally dependant: -q -v results in
        loglevel 'error'

    --help, -h
        print this message and exit

    --output, -o ( none | - | exec <prog> [<arg1> [<arg2> ...] ] )
        determines what to do with normal output - by default, stee writes to
        stdout (like normal tee), -o none disables normal output (stee only
        writes to file(s)), -o exec <prog> ... starts <prog> and pipes normal
        output to it (functionally equivalent to "stee [...] | <prog> in"
        a unix shell)
```


DEPENDENCIES
------------
GCC, cstdlib (clang should work too, update Makefile if you want to use it)

BUILD
-----
Build with `make [DEBUG=1]`

INSTALL
-------
`make install [DESTDIR=<destdir>]` (default `DESTDIR` is `/usr`)

Uninstall with `make uninstall [DESTDIR=<destdir>]`

