#!/bin/bash

tmpdir=/tmp/stee-test-$$
mkdir "$tmpdir"
ctl="$tmpdir"/ic.ctl
prog='./bin/stee'

function test_writer() {
	i=0
	while true; do
		echo $((i++))
		sleep 2
	done
}

# functionality test
function test_1() {
	echo 'starting process (with --no-write)'
	test_writer | \
		$prog -n -f"${tmpdir}/stee_%n_%T.out" -c"$ctl" -Linfo  -s7 \
			-oexec cat > "$tmpdir/ic.out" &

	sleep 1; echo rec...     ;printf r > "$ctl"
	sleep 2; echo pause...   ;printf p > "$ctl"
	sleep 2; echo rec...     ;printf r > "$ctl"
	sleep 2; echo new file...;printf n > "$ctl"
	sleep 2; echo stop...    ;printf s > "$ctl"
	sleep 2; echo start...   ;printf r > "$ctl"
	sleep 1
	echo 'wait for automatic creation of new file (-split 7)'...
	sleep 11; echo quit...    ;printf q > "$ctl"

	sleep 1

	echo
	echo 'checking correctness...'
	if \
		diff -u "$tmpdir/stee_01_"* tests/stee_01_* && \
		diff -u "$tmpdir/stee_02_"* tests/stee_02_* && \
		diff -u "$tmpdir/stee_03_"* tests/stee_03_* && \
		diff -u "$tmpdir/stee_04_"* tests/stee_04_* && \
		diff -u "$tmpdir/ic.out"    tests/ic.out
	then echo ok
	else echo "output doesn't match"
	fi
}

# bad arguments test
function test_2() {
	echo
	echo testing bad args...

	if $prog --split 292jksj            || \
		$prog jkl%kja%nNui%v || \
		$prog -ojaskfczd     || \
		$prog --loglevel sjfklshfi  || \
		$prog > /dev/null
	then echo "something returned 0 when it souldn't"
	else echo ok
	fi
}

#performance test
function test_3() {
	test_nmbs=1024

	echo
	echo "testing performance: write ${test_nmbs}MB (from /dev/zero to /dev/null)"
	echo 'file and stdout...'
	time dd if=/dev/zero bs=1024k count=$test_nmbs 2> /dev/null | \
		$prog -v -o- /dev/null > /dev/null

	echo
	echo 'only file...'
	time dd if=/dev/zero bs=1024k count=$test_nmbs 2> /dev/null | \
		$prog -v -onone /dev/null

	echo
	echo 'file, stdout, with ctl...'
	time dd if=/dev/zero bs=1024k count=$test_nmbs 2> /dev/null | \
		$prog -v -c"$ctl" -o- /dev/null > /dev/null
}

if [ "$#" -eq 0 ];then
	test_1
	test_2
	test_3
else
	while [ "$#" -gt 0 ];do case "$1" in
		1|2|3) test_$1;shift;;
		*) break;;
	esac;done
fi

rm -r "$tmpdir"
