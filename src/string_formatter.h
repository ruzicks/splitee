/*
 * Module for formatting filenames
 */
#ifndef	STRING_FORMATTER_H
#define	STRING_FORMATTER_H

struct f_str {
	char *str;	/* the resulting string		*/
	void *spec;	/* internal formatter data	*/
};

/* initializes the structure */
void
f_str_init(struct f_str *f_string, char *format);

/*
 * sets next string, freeing the old one
 * returns -1 on error, >= 0 otherwise
 */
int
f_str_next(struct f_str *f_string);

/*
 * sets next string, and returns the old one or NULL on error
 */
char
*f_str_next_pop(struct f_str *f_string);

void
f_str_free(struct f_str *f_string);
#endif
