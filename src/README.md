# SPLITEE - SPLITTING TEE

## Compilation

Compile with `make [DEBUG=1] [CC=<compiler>]`. Requires `_POSIX_C_SOURCE >=
200112L`, `_XOPEN_SOURCE >= 600` and `c99`. Other combination may work on some
systems, this setup is tested on Solaris (`gcc` and `cc`) and Linux (`clang`,
`gcc`). Use `DEBUG=1` for debug build. If `CC` is neither of `cc`, `gcc`,
`clang` then some `CFLAGS` will be missing (c standard (`c99`), optimization,
warning level, debug. info). Additionally, flags defined in `CFLAGS_EXTRA` will
be used. You can run correctness and performance tests with `./test.sh`

## Synopsis
```bash
stee --file <filename> [--no-write] [--split <interval>] [--ctl <ctl_path>]
   [--log <logfile>] [--loglevel <loglevel>] [--verbose] [--quiet] [--help]
   [--output ( none | - | exec <prog> [<arg1> [<arg2> ...] ] ) ]

stee [...] <filename>
```

The program reads data from standard input and writes it to file(s) and either
standard output (`--output -`) or pipes it to specified program (`--output exec
<prog> ...`) or neither.

##### writing to file(s)
The data can be split into multiple files, always creating new file after
specified time interval (`--split`), or by issuing command through _fifo ctl_ to
create new file. That can be created by using the `--ctl <ctlpath>` option and
can be used to instruct the program to start/stop writing into the file or to
create a new file (the program will still keep writing the data to standard
output/_exec program_'s input, if appropriate options were used). Names of
output files are determined by _pattern_, which can be specified as the last
argument or set with the `--file` option (needed with `--output exec`).

## Options

* `-f`, `--file <pattern>`
    Pattern for file name(s) where data will be written.
    Accepts following formating sequences:
    - `%n` -> file number (`printf` format `%02d`), useful when splitting input
      into more files)
    - `%p` -> process id
    - `%u` -> (real) user id
    - `%T` -> current time HH:MM:SS
    - `%D` -> current date YYYY-MM-DD
    - `%H` -> hour
    - `%M` -> minute
    - `%S` -> second
    - `%s` -> unix timestamp
    - `%%` -> %

* `-n`, `--no-write`
    Don't automatically start writing into output file after launch. Usefull
    with `--ctl` (see the `r` ctl command)

* `-s`, `--split <interval>`
    If nonzero, new file will be created each `<interval>` seconds.
    Default is 0 (= no splitting).
* `-c`, `--ctl <ctl_path>`
    Program creates named pipe in location `<ctl_path>` (which accepts the same
    formatting sequences as `--file`) and listens for commands. Following
    commands are currently supported:
     - `n` - create new file
     - `r` - start recording (has no effect if already recording)
     - `s` - stop recording (close the file - new file will be created by `r`)
     - `p` - pause recording (`r` will resume writing into the same file)
     - `q` - quit

   Other characters are ignored.
* `-o`, `--output none`
    Program only writes to file(s).
* `-o`, `--output -`
    Program also writes data to stdout. (default)
* `-o`, `--output exec <prog> [<arg1>] [<arg2>] ...`

    Executes `<prog>` with arguments `<arg1>` `<arg2>` `...` and pipes data to
    its standard input  (in addition to writing to file(s)).  This must be the
    last argument - anything that follows is treated as arguments for `<prog>`
    (that means that you have to specify `<pattern>` using the `--file` option).
    When exitting, closes `<prog>`'s stdin and waits for it to exit.
* `-l`, `--log <logfile>`
    Program will use `<logfile>` for logging, appending at its end. If the
    file doesn't exist, it will be created.
* `-L`, `--loglevel <level>`
    Changes loglevel to `<level>`. Allowed levels are `quiet`, `error`, `info`
    (default), `debug`
* `-v`, `--verbose`
    Increases verbosity level by 1.
* `-q`, `--quiet`
    Alias for `--loglevel quiet --output none`.

##### `stee` is NOT
- in any way, shape or form intended for filtering or analyzing the data that is
  read - it may split the data into files in inconvenient places
- meant for generation or creation of folder structure, name formatting
  sequences should only be used for filenames and preexisting folders (this
  might be a worthy feature to add in the future)
- meant to be resilient or smart about writing data to output or files and may
  block if output is not being consumed fast enough or writing to file blocks;
  it may quit if problem is encountered in either writing to file or to output,
  even if one of those is still working

## Implementation

The program is divided into several files:
* `input_capture.c` - manages the main loop
* `ic_ctl.c` - control "channel" for receiving commands (since standard input
  can't be used for that), implemented as named pipe (fifo)
* `string_formatter.c` - generates string(s) from `<pattern>` (for filenames)

`main.c` parses the arguments and prepares output (in case of `--output exec`
forks and exec-s the program) and initializes `input_capture` with given input
and output (the program can therefore be used as a library, with arbitrary input
and output). The main loop takes care of managing `ctl` and input/output/files
in one thread.  `ctl` interface provides the read fd, the loop checks this file
descriptor and data input fd with `select()` and handles the commands from
`ctl`/data on stdin when they're ready. Reading and writing is done with
`read`/`write`.  The individual interfaces are prepared to run `ctl` in separate
thread which could then "poke" specified thread with `SIGUSR1` (this would be
the main loop thread), but the potential gains are questionable, and it
introduces a race condition (between `SIGUSR1` and `select()`). That can be
solved with signal masking and `pselect()`, but the preferred solution was to
handle everything sequentially from one thread (simpler and there are no real
downsides for typical usage). `ctl` itself is fairly contained and only exposes
`unsiged int cmd` - or-ed commands for the program.
