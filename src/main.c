#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <pthread.h>

#include "input_capture.h"
#include "logger.h"

#define	DEFAULT_FORMAT "pipe_capture-part_%n.stream"

enum OUTPUT {NONE = 0, STDOUT, EXEC};

int *sigflag_exit = NULL;
int sig = -1;

void
sig_exit(int sig)
{
	*sigflag_exit = 1;
#ifdef DEBUG
	fprintf(stderr, "received signal %d: exiting\n", sig);
#endif
}

void
sig_void(int sig) {}

void
usage(FILE *stream, char *prg_name)
{
	fprintf(stream,
"usage:\n"
"%s --file <filename> [--no-write] [--split <interval>] [--ctl <ctl_pattern>]\n"
"   [--log <logfile>] [--loglevel <loglevel>] [--verbose] [--quiet] [--help]\n"
"   [--output ( none | - | exec <prog> [<arg1> [<arg2> ...] ] ) ]\n"
"\n"
"%s [...] <filename>\n"
"\n"
"   <filename> formating sequences: %%n (file number), %%T (time HH:MM:SS),\n"
"      %%D (date YYYY-MM-DD), %%H (hour), %%M (minute), %%S (second),\n"
"      %%p (PID), %%u (UID), %%s (unix timestamp), %%%% (%%)\n"
"   ctl command characters: 'n' (create new file), 'r' (start recording),\n"
"      's' (stop recording), 'p' (pause recording), 'q' (quit)\n"
"\n"
"    --file <filename>\n"\
"        specifies output file pattern\n"
"\n"
"    --no-write, -n\n"
"        don't immediately write to file, wait for fifo ctl 'r' command\n"
"\n"
"    --split, -s <interval>\n"
"        when writing to file, creating new file every <interval> seconds\n"
"\n"
"    --ctl, -c <ctl_pattern>\n"
"        create fifo ctl at <ctl_pattern> (uses same formatting characters as\n"
"        output <filename>\n"
"\n"
"    --log, -l <logfile>\n"
"        log to <logfile> instead of stderr\n"
"\n"
"    --loglevel, -L <loglevel>\n"
"        one of quiet, error, info, debug (defaults to info)\n"
"\n"
"    --quiet, -q\n"
"        sets loglevel to quiet\n"
"\n"
"    --verbose, -v\n"
"        increases loglevel (positionally dependant: -q -v results in\n"
"        loglevel 'error'\n"
"\n"
"    --help, -h\n"
"        print this message and exit\n"
"\n"
"    --output, -o ( none | - | exec <prog> [<arg1> [<arg2> ...] ] )\n"
"        determines what to do with normal output - by default, stee writes to\n"
"        stdout (like normal tee), -o none disables normal output (stee only\n"
"        writes to file(s)), -o exec <prog> ... starts <prog> and pipes normal\n"
"        output to it (functionally equivalent to \"stee [...] | <prog>\" in\n"
"        a unix shell)\n"
"\nFor more details read README.md\n",
	prg_name, prg_name);
}

int
main(int argc, char **argv)
{
	/* format string for name(s) of output file(s) */
	char	*name_string = NULL;
	/* 0 -> don't start writing into file(s), wait on ctl cmd */
	int	do_write = 1;
	ic_data ic;			/* input capture data		*/
	int	filedes[2];		/* pipe to exec-ed program	*/
	int	output = STDOUT;	/* type of non-file output	*/
	int	output_fd = 1;		/* filedes of non-file output	*/
	/* int	exec_argc = 0;		* argc for exec-ed program	*/
	int	exec_pid = 0;		/* pid of exec-ed program	*/
	char	**exec_argv = NULL;	/* argv for exec-ed program	*/
	char	*ctl_path = NULL;	/* fifo path for ctl		*/
	int	split_interval = 0;	/* create new file each N sec.	*/

	sigflag_exit = &ic.sigflag_exit;

	FILE *logfile = stderr;
	int loglevel = LOGLEVEL_INF;
	logger = NULL;	/* logger */

	if (argc < 2) {
		usage(stdout, argv[0]);
		return (1);
	}

	/*
	 * GETOPTS
	 */

	int opt = -1;
	int option_index = 0;

	struct sigaction sig;
	sigset_t sigs;
	sigemptyset(&sigs);

	sig.sa_handler = &sig_exit;
	sig.sa_mask = sigs;
	sig.sa_flags = 0;

	sigaction(SIGHUP, &sig, NULL);
	sigaction(SIGINT, &sig, NULL);
	sigaction(SIGPIPE, &sig, NULL);
	sigaction(SIGTERM, &sig, NULL);
	sigaction(SIGCHLD, &sig, NULL);

	/*
	 * sig.sa_handler = &sig_void;
	 * sigaction(SIGUSR1, &sig, NULL);
	 */

	struct option options[] = {
		{"file",	required_argument,	0, 'f'},
		{"no-write",	no_argument,		0, 'n'},
		{"split",	required_argument,	0, 's'},
		{"output",	required_argument,	0, 'o'},
		{"ctl",		required_argument,	0, 'c'},
		{"log",		required_argument,	0, 'l'},
		{"loglevel",	required_argument,	0, 'L'},
		{"verbose",	no_argument,		0, 'v'},
		{"quiet",	no_argument,		0, 'q'},
		{"help",	no_argument,		0, 'h'},
		{0, 0, 0, 0 }
	};
	while ((opt = getopt_long(
		    argc, argv, "f:ns:o:c:l:L:vqh", options, &option_index))
	    != -1) {
		char *endptr;
		switch (opt) {
		case 'h':
			usage(stdout, argv[0]);
			exit(0);
		case 'f':
			/* -name formatstring (format string for output name) */
			name_string = optarg;
			break;
		case 'n':
			do_write = 0;
			break;
		case 's':
			/*
			 * -split duration
			 * split input into files,
			 * making new file each `duration' seconds
			 */
			split_interval = strtol(optarg, &endptr, 10);
			if (*endptr != '\0') {
				fprintf(
				    stderr,
				    "invalid split interval: `%s'\n",
				    optarg);
				exit(1);
			}
			break;
		case 'c':
			/*
			 * creat named pipe for reading commands
			 * (new-file/stop recording/etc)
			 */
			ctl_path = optarg;
			break;
		case 'l':
			if ((logfile = fopen(optarg, "a")) == NULL) {
				perror("trying to open logfile");
				exit(1);
			}
			break;
		case 'L':
			if (strcmp(optarg, "quiet") == 0)
			    loglevel = LOGLEVEL_NON;
			else if (strcmp(optarg, "error") == 0)
			    loglevel = LOGLEVEL_ERR;
			else if (strcmp(optarg, "info") == 0)
			    loglevel = LOGLEVEL_INF;
			else if (strcmp(optarg, "debug") == 0)
			    loglevel = LOGLEVEL_DBG;
			else {
				fprintf(stderr, "invalid loglevel: `%s'\n",
					optarg);
				exit(1);
			}
			break;
		case 'v':
			++loglevel;
			break;
		case 'q':
			loglevel = LOGLEVEL_NON;
			output = NONE;
			break;
		case 'o':
			if (strcmp(optarg, "-") == 0)
				/* -output - pipe processed input to stdout */
				output = STDOUT;
			else if (strcmp(optarg, "none") == 0)
				/* -output none only writes to file(s) */
				output = NONE;
			else if (strcmp(optarg, "exec") == 0) {
				/*
				 * --output program [arg1 [arg2 ...]]
				 * calls `program' with arguments
				 * and pipes input to program's stdin,
				 * arguments are passed separately
				 */
				output = EXEC;
				/* exec_argc = argc - optind; */
				exec_argv = argv + optind;
				goto finish_getopt;
			} else {
				fprintf(
				    stderr,
				    "wrong argument for -output: `%s'\n",
				    optarg);
				exit(1);
			}
			break;
		default:
			usage(stderr, argv[0]);
			exit(1);
		}
	}
finish_getopt:

	/*
	 * PREPARING THINGS FOR THE MAIN LOOP
	 */

	if (output != EXEC && optind < argc) {
		name_string = argv[optind++];
		if (optind < argc) {
			fprintf(stderr, "invalid trailing arguments\n");
			exit(1);
		}
	}

	/* prepare logger */
	if (logfile == NULL) {
		loglevel = LOGLEVEL_NON;
	}
	if ((logger = logger_init(logfile, loglevel)) == NULL) {
		fprintf(stderr, "error initializing logger\n");
		exit(1);
	}

	/* check <pattern> correctness */
	if (name_string) {
		struct f_str fstr;
		f_str_init(&fstr, name_string);
		if (f_str_next(&fstr) == -1) {
			log(LOGLEVEL_ERR,
			    "bad name format: `%s'\n",
			    name_string);
			exit(1);
		}
		f_str_free(&fstr);
	} else {
		log(LOGLEVEL_ERR, "you have to specify output file pattern");
		exit(1);
	}

	/* handle non-file output stream */
	if (output == EXEC) {
		if (pipe(filedes) == -1) {
			log(LOGLEVEL_ERR,
			    "preparing exec `%s': "
			    "trying to create pipe failed: %s",
			    exec_argv[0],
			    strerror(errno));
			exit(1);
		}
		output_fd = filedes[1];
		switch (exec_pid = fork()) {
		case -1:
			log(LOGLEVEL_ERR,
			    "preparing exec `%s': fork: %s",
			    exec_argv[0],
			    strerror(errno));
			exit(1);
		case 0:
			close(filedes[1]);
			close(0);
			if (dup(filedes[0]) == -1) {
				log(LOGLEVEL_ERR,
				    "preparing exec `%s': dup: %s",
				    exec_argv[0],
				    strerror(errno));
				exit(1);
			}
			if (execvp(exec_argv[0], exec_argv) == -1) {
				log(LOGLEVEL_ERR,
				    "execvp `%s': %s",
				    exec_argv[0],
				    strerror(errno));
				exit(1);
			}
		default:
			close(filedes[0]);
			break;
		}
	} else if (output == STDOUT) {
		output_fd = fileno(stdout);
	}

	int res = ic_init(
	    &ic,
	    0,
	    output ? output_fd : -1,
	    name_string,
	    ctl_path,
	    split_interval);
	if (res != -1) {
		ic.do_capture = do_write;
		res = ic_main_loop(&ic);
		log(LOGLEVEL_DBG, "exiting");
		ic_free(&ic);
	} else log(LOGLEVEL_DBG, "failed to initialize - exiting");

	if (output) {
		close(output_fd);
	}
	if (exec_pid) {
		if (output == EXEC) {
			int status;
			waitpid(exec_pid, &status, 0);
			if (status != 0) {
				log(LOGLEVEL_ERR,
				    "execed program `%s' returned %d",
				    exec_argv[0],
				    status);
				res = status;
			}
		}
	}
	if (logger) logger_free(logger);

	return (res == 0 ? 0 : 1);
}
