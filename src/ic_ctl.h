/*
 * Module for communication - accepting commands. This implementation uses
 * named pipes, but apart from creating the ctl, the API can be easily
 * implemented in other ways.
 *
 * TODO: independent ctl_free function by adding destructor to the struct
 */
#ifndef	IC_FIFO_CTL_H
#define	IC_FIFO_CTL_H

#include <stdlib.h>
#ifdef HAVE_PTHREAD
#include <pthread.h>
#endif

/* TODO: this shuld be defined by user of the module */
#define	CTL_CHAR_EXIT		'q'
#define	CTL_CHAR_NEW_FILE	'n'
#define	CTL_CHAR_REC_START	'r'
#define	CTL_CHAR_REC_PAUSE	'p'
#define	CTL_CHAR_REC_STOP	's'

#define	CTL_EXIT	0x01
#define	CTL_NEW_FILE	0x02
#define	CTL_REC_START	0x04
#define	CTL_REC_PAUSE	0x08
#define	CTL_REC_STOP	0x10
#define	CTL_REC		(CTL_REC_START | CTL_REC_PAUSE | CTL_REC_STOP)
#define	CTL_NONE	0x00

#define	FIFO_CTL_BUFSIZE 32

struct ic_ctl {
#ifdef HAVE_PTHREAD
	pthread_mutex_t lck;
#endif
	unsigned int cmd;	/* command character */
	int	read_fd;	/* ctl read file descriptor or (-1) */
	void	*ctl_data;	/* internal data of ctl implementation */
};

/* Returns commands (or-ed commands CTL_xxx) and resets it to zero. */
unsigned int
ctl_pop_cmd_r(struct ic_ctl *ctl);

/*
 * reads input and sets ctl->cmd appropriately
 * you can use select() (or equiv.) on ctl->read_fd to see whether it will
 * block
 */
int
ctl_read(struct ic_ctl *ctl);

/*
 * Creates new fifo ctl
 * returns 0 on success -1 or error code on error
 */
int
fifo_ctl_init(struct ic_ctl *ctl, char *fifo_path);


#ifdef HAVE_PTHREAD
/*
 * If notify_thr is not NULL, will call pthread_kill(*notify_thr, SIGUSR1)
 * upon receiving controll command
 */
void
fifo_ctl_set_notify_thread(struct ic_ctl *ctl, pthread_t *notify_thr);

/*
 * Starts fifo read loop in a separate thread, which always sets appropriate
 * cmd flags and sends SIGUSR1 signal to notify_thr if it's not NULL
 *
 * returns 0 on success, nonzero/error code on failure
 */
int
fifo_ctl_start(struct ic_ctl *ctl);
#endif /* HAVE_PTHREAD */

/*
 * Stops the listening thread and cleans up all resources
 * (not the struct ic_ctl *ctl itself).
 */
void
fifo_ctl_free(struct ic_ctl *ctl);

#endif /* IC_FIFO_CTL_H */
