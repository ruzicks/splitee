#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <sys/select.h>
#include <assert.h>

#include "input_capture.h"
#include "logger.h"

#define	MAX(A, B) (A > B ? A : B)
#ifndef IC_BUFSIZE
#define	IC_BUFSIZE 4096
#endif


static void
ic_close_file(ic_data *ic)
{
	if (ic->capture_file) {
		fclose(ic->capture_file);
		log(
		    LOGLEVEL_INF,
		    "capturing to file '%s' finished",
		    ic->file_str.str);
		ic->capture_file = NULL;
	}
}

/* 0 on success, -1 on error */
static int
ic_next_file(ic_data *ic)
{
	int ret = 0;
	ic->last = time(NULL);
	if (ic->capture_file) ic_close_file(ic);
	if (f_str_next(&ic->file_str) == -1) {
		log(LOGLEVEL_ERR, "failed to generate filename from pattern");
		return (-1);
	}
	ic->capture_file = fopen(ic->file_str.str, "a+");
	if (!ic->capture_file) {
		log_perror(LOGLEVEL_ERR, "fopen");
		log(LOGLEVEL_ERR,
		    "failed to create file `%s'",
		    ic->file_str.str);
		ret = -1;
	}
	log(LOGLEVEL_INF,
	    "started capturing to new file `%s'",
	    ic->file_str.str);
	return (ret);
}

static void
ic_handle_ctl_cmd(ic_data *ic)
{
	if (!ic->ctl) {
	    return;
	}
	ctl_read(ic->ctl);
	unsigned int cmd = ic->ctl->cmd;
	ic->ctl->cmd = CTL_NONE;
	if (cmd & CTL_EXIT) ic->sigflag_exit = 1;
	if ((cmd & CTL_REC_START) && !ic->do_capture) {
		log(LOGLEVEL_DBG, "cmd_start");
		if (ic->capture_file) {
			log(LOGLEVEL_INF,
			    "resuming capturing to file `%s'",
			    ic->file_str.str);
		}
		ic->do_capture = 1;
	} else if (cmd & CTL_REC_STOP) {
		log(LOGLEVEL_DBG, "cmd_stop");
		ic_close_file(ic);
		ic->do_capture = 0;
	} else if (cmd & CTL_REC_PAUSE) {
		log(LOGLEVEL_DBG, "cmd_pause");
		ic->do_capture = 0;
		if (ic->capture_file) {
			fflush(ic->capture_file);
			log(LOGLEVEL_INF,
			    "paused capturing to file `%s'",
			    ic->file_str.str);
		}
	} if (ic->capture_file && (cmd & CTL_NEW_FILE)) {
		log(LOGLEVEL_DBG, "cmd_new_file");
		/* next file will be created on first following write */
		ic_close_file(ic);
	}
}

int
ic_init(
    ic_data *ic,
    int read_fd,
    int write_fd,
    char *filename_format,
    char *ctl_path,
    int split_interval)
{
	ic->read_fd = read_fd;
	ic->write_fd = write_fd;
	ic->split_interval = split_interval;
	ic->do_capture = 1;
	ic->sigflag_exit = 0;

	if (ctl_path) {
		struct f_str ctl_path_pattern;
		f_str_init(&ctl_path_pattern, ctl_path);
		if (-1 == f_str_next(&ctl_path_pattern)) {
			log(LOGLEVEL_ERR,
			    "`%s': invalid ctl path pattern",
			    ctl_path);
			f_str_free(&ctl_path_pattern);
			return (-1);
		}
		ic->ctl = calloc(1, sizeof (struct ic_ctl));
		int res = fifo_ctl_init(ic->ctl, ctl_path_pattern.str);
		f_str_free(&ctl_path_pattern);
		if (res != 0) {
			log(
			    LOGLEVEL_ERR,
			    "unable to create fifo controller: "
			    "fifo_ctl_init returned `%d'",
			    res);
			free(ic->ctl);
			ic->ctl = NULL;
			return (-1);
		}
	} else ic->ctl = NULL;

	f_str_init(&ic->file_str, filename_format);
	ic->capture_file = NULL;
	return (0);
}

int
ic_main_loop(ic_data *ic)
{
	void	*buf = malloc(IC_BUFSIZE);	/* read buffer	*/
	ssize_t	read_len = 0;		/* number of bytes read	*/
	fd_set	fdset_read, fdset_tmp;	/* fd_set w/ stdin - for select	*/
	int fds_max;			/* max. filedes for select() +1	*/

	FD_ZERO(&fdset_read);
	FD_SET(ic->read_fd, &fdset_read);
	fds_max = ic->read_fd + 1;
	if (ic->ctl) {
		FD_SET(ic->ctl->read_fd, &fdset_read);
		fds_max = MAX(fds_max, ic->ctl->read_fd + 1);
	}

	while (!ic->sigflag_exit) {
		fdset_tmp = fdset_read;
		int select_res = select(fds_max, &fdset_tmp, NULL, NULL, NULL);
		if (select_res == -1) {
			if (errno != EINTR) {
				log_perror(
				    LOGLEVEL_ERR,
				    "select() on stdin/ctl");
				break;
			} else continue;
		} else if (select_res == 0) {
			continue;
		}
		assert(select_res > 0);

		time_t tm_now = time(NULL);
		if (ic->split_interval && ic->capture_file &&
		    tm_now - ic->last >= ic->split_interval) {
			ic_close_file(ic);
		}

		if (ic->ctl && FD_ISSET(ic->ctl->read_fd, &fdset_tmp)) {
			ic_handle_ctl_cmd(ic);
		}

		if (FD_ISSET(ic->read_fd, &fdset_tmp)) {
			read_len = read(ic->read_fd, buf, IC_BUFSIZE);
			if (read_len < 0) {
				log_perror(
				    LOGLEVEL_ERR,
				    "trying to read from stdin");
				break;
			} else if (read_len == 0) {
				ic->sigflag_exit = 1;
				break;
			}
			if (ic->write_fd >= 0 &&
			    write(ic->write_fd, buf, read_len) == -1) {
				log_perror(
				    LOGLEVEL_ERR,
				    "trying to write to output");
				break;
			}

			if (ic->do_capture) {
				if (!ic->capture_file &&
				    ic_next_file(ic) == -1) {
					break;
				}
				if (fwrite(buf, 1, read_len, ic->capture_file)
				    != read_len) {
					log_perror(
					    LOGLEVEL_ERR,
					    "trying to write to output file");
					break;
				}
			}
		}
	}
	free(buf);
	return (ic->sigflag_exit ? 0 : -1);
}

void
ic_free(ic_data *ic)
{
	if (ic->ctl) {
		fifo_ctl_free(ic->ctl);
		free(ic->ctl);
	}
	if (ic->capture_file) {
		fclose(ic->capture_file);
		log(
		    LOGLEVEL_INF,
		    "capturing to file '%s' finished",
		    ic->file_str.str);
	}
	f_str_free(&ic->file_str);
}
