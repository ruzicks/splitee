#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <stdarg.h>

#ifdef HAVE_PTHREAD
#include <pthread.h>
#endif

#include "logger.h"

struct logger *logger;

const char *loglevel_str[] = {
	"critical",
	"error",
	"info",
	"debug"
};

struct logger {
	FILE *logfile;
	int loglevel;
#ifdef HAVE_PTHREAD
	pthread_mutex_t lock;
#endif
};

struct logger *
logger_init(FILE *file, int loglevel)
{
	struct logger *ret = calloc(1, sizeof (struct logger));
	if (loglevel == LOGLEVEL_NON || file == NULL) {
		ret->loglevel = LOGLEVEL_NON;
		return (ret);
	}
	ret->logfile = file;
	ret->loglevel = loglevel;
#ifdef HAVE_PTHREAD
	int err;
	if ((err = pthread_mutex_init(&ret->lock, NULL)) != 0) {
		fprintf(
		    stderr,
		    "%s: %d: initializing logger: %s\n",
		    __FILE__,
		    __LINE__,
		    strerror(err));
		if (ret->logfile) fclose(ret->logfile);
		free(ret);
		return (NULL);
	}
#endif
	return (ret);
}

void
logger_free(struct logger *l)
{
#ifdef HAVE_PTHREAD
	pthread_mutex_destroy(&l->lock);
#endif
	if (l->logfile) fclose(l->logfile);
	free(l);
}

void
log_write(struct logger *l, int loglevel, char *file, int line, char *fmt, ...)
{
	if (l->loglevel < loglevel) {
		return;
	}
#ifdef HAVE_PTHREAD
	pthread_mutex_lock(&l->lock);
#endif
	va_list arglist;
	va_start(arglist, fmt);
	fprintf(l->logfile, "%s: ", loglevel_str[loglevel]);
	if (loglevel == LOGLEVEL_ERR || loglevel == LOGLEVEL_DBG) {
		fprintf(l->logfile, "%s: %d: ", file, line);
	}

	vfprintf(l->logfile, fmt, arglist);
	fprintf(l->logfile, "\n");
	fflush(l->logfile);
	va_end(arglist);
#ifdef HAVE_PTHREAD
	pthread_mutex_unlock(&l->lock);
#endif
}

void
log_fileline(struct logger *l, char *file, int line)
{
	fprintf(l->logfile, "%s: %d: ", file, line);
}
