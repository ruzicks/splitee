#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#ifdef HAVE_PTHREAD
#include <pthread.h>
#endif

#include "ic_ctl.h"
#include "logger.h"

#define	 MSG_SIZE 255

typedef struct {
	char *fifo_path;
	int fifo_fd_write;
#ifdef HAVE_PTHREAD
	pthread_t *loop_thr;
	pthread_t *notify_thr;
#endif
	char stop_flag;
} fifo_ctl_data;

unsigned int
ctl_pop_cmd_r(struct ic_ctl *ctl)
{
	unsigned int ret;
#ifdef HAVE_PTHREAD
	pthread_mutex_lock(&ctl->lck);
#endif
	ret = ctl->cmd;
	ctl->cmd = CTL_NONE;
#ifdef HAVE_PTHREAD
	pthread_mutex_unlock(&ctl->lck);
#endif
	return (ret);
}

static void
setcmd(struct ic_ctl *ctl, char c)
{
	switch (c) {
	case CTL_CHAR_EXIT:
		ctl->cmd |= CTL_EXIT;
		break;
	case CTL_CHAR_NEW_FILE:
		ctl->cmd |= CTL_NEW_FILE;
		break;
	case CTL_CHAR_REC_START:
		if (ctl->cmd & CTL_REC_STOP) ctl->cmd |= CTL_NEW_FILE;
		ctl->cmd = (ctl->cmd & ~CTL_REC) | CTL_REC_START;
		break;
	case CTL_CHAR_REC_STOP:
		ctl->cmd = (ctl->cmd & ~CTL_REC) | CTL_REC_STOP;
		break;
	case CTL_CHAR_REC_PAUSE:
		if (ctl->cmd & CTL_REC_STOP) break;
		ctl->cmd = (ctl->cmd & ~CTL_REC) | CTL_REC_PAUSE;
		break;
	default: c = 0;
	}

#ifdef HAVE_PTHREAD
	fifo_ctl_data *data = ctl->ctl_data;
	if (c != 0 && data->notify_thr) {
		pthread_kill(*(data->notify_thr), SIGUSR1);
	}
#endif
}

#ifdef HAVE_PTHREAD
static void *
fifo_ctl_loop(void *arg)
{
	struct ic_ctl *ctl = arg;
	fifo_ctl_data *data = ctl->ctl_data;

	char buf[FIFO_CTL_BUFSIZE];
	while (!data->stop_flag) {
		int n = read(ctl->read_fd, (void*) &buf, FIFO_CTL_BUFSIZE);
		if (data->stop_flag) {
			return (NULL);
		} else if (n == -1) {
			log_perror(
			    LOGLEVEL_ERR,
			    "ctl: trying to read from fifo");
			return (NULL);
		} else if (n == 0) {
			log(LOGLEVEL_ERR,
			    "ctl fifo: read returned EOF - "
			    "this should never happen");
		}
		pthread_mutex_lock(&ctl->lck);
		int i;
		for (i = 0; i < n; ++i) {
			setcmd(ctl, buf[i]);
		}
		pthread_mutex_unlock(&ctl->lck);
	}
	return (NULL);
}
#endif

int
ctl_read(struct ic_ctl *ctl)
{
	char buf[FIFO_CTL_BUFSIZE];
	int n = read(ctl->read_fd, (void*) &buf, FIFO_CTL_BUFSIZE);
	if (n == -1) {
		log_perror(
		    LOGLEVEL_ERR,
		    "ctl: trying to read from fifo");
		return (-1);
	} else if (n == 0) {
		log(LOGLEVEL_ERR,
		    "ctl: read returned EOF - "
		    "this should never happen");
		return (-1);
	}
	int i;
	for (i = 0; i < n; ++i) {
		setcmd(ctl, buf[i]);
	}
	return (0);
}

int
fifo_ctl_init(struct ic_ctl *ctl, char *fifo_path)
{
	int ret = 0;
	if (mkfifo(fifo_path, 0666) == -1) {
		int ret = errno;
		log_perror(
		    LOGLEVEL_ERR,
		    "ctl: failed to create fifo");
		errno = 0;
		return (ret);
	}

	memset(ctl, 0, sizeof (struct ic_ctl));
	fifo_ctl_data *data = calloc(1, sizeof (fifo_ctl_data));

#ifdef HAVE_PTHREAD
	data->notify_thr = NULL;
#endif
	data->fifo_path = strdup(fifo_path);
	data->stop_flag = 0;

	/*
	 * open fifo
	 * open for reading in nonblocking mode, open for writing
	 * (which should not block, since it is already opened for reading)
	 * and then set read fd to blocking. This has essentially the same
	 * effect as O_RDWR on linux, however that is not specified by POSIX
	 *
	 * fifo has to be opened for writing, otherwise read keeps returning
	 * EOF (=busy waiting)
	 */
	ctl->read_fd = open(data->fifo_path, O_RDONLY | O_NONBLOCK);
	if (ctl->read_fd == -1) {
		ret = errno;
		log_perror(
		    LOGLEVEL_ERR,
		    "ctl: trying to open fifo for reading");
		goto ic_ctl_init_cleanup;
	}
	data->fifo_fd_write = open(data->fifo_path, O_WRONLY);
	if (data->fifo_fd_write == -1) {
		ret = errno;
		log_perror(
		    LOGLEVEL_ERR,
		    "ctl: trying to open fifo for writing");
		goto ic_ctl_init_cleanup;
	}
	int flags = fcntl(ctl->read_fd, F_GETFL);
	if (fcntl(ctl->read_fd, F_SETFL, flags & ~O_NONBLOCK) == -1) {
		ret = errno;
		log_perror(
		    LOGLEVEL_ERR,
		    "ctl: setting up fifo: fcntl(..., F_SETFL,...)");
		goto ic_ctl_init_cleanup;
	}

#ifdef HAVE_PTHREAD
	data->loop_thr = NULL;
	pthread_mutex_init(&ctl->lck, NULL);
#endif

	ctl->ctl_data = data;
	return (0);

ic_ctl_init_cleanup:
	if (ctl->read_fd != -1) {
		close(ctl->read_fd);
		if (data->fifo_fd_write != -1) close(data->fifo_fd_write);
	}
	unlink(data->fifo_path);
	free(data->fifo_path);
	free(data);
	ctl->ctl_data = NULL;
	return (ret);
}

#ifdef HAVE_PTHREAD
void
fifo_ctl_set_notify_thread(struct ic_ctl *ctl, pthread_t *notify_thr)
{
	fifo_ctl_data *data = ctl->ctl_data;
	data->notify_thr = notify_thr;
}

int
fifo_ctl_start(struct ic_ctl *ctl)
{
	fifo_ctl_data *data = ctl->ctl_data;
	data->loop_thr = malloc(sizeof (pthread_t));
	int ret;
	ret = pthread_create(data->loop_thr, NULL, &fifo_ctl_loop, ctl);
	if (ret != 0) {
		fprintf(
		    stderr,
		    "fifo_ctl_start: pthread create returned %d\n",
		    ret);
		return (ret);
	}
	return (0);
}
#endif /* HAVE_PTHREAD */

void
fifo_ctl_free(struct ic_ctl *ctl)
{
#ifdef HAVE_PTHREAD
	pthread_mutex_lock(&ctl->lck);
#endif
	fifo_ctl_data *data = ctl->ctl_data;
	data->stop_flag = 1;
	close(data->fifo_fd_write);
	close(ctl->read_fd);
	unlink(data->fifo_path);
#ifdef HAVE_PTHREAD
	if (data->loop_thr) pthread_kill(*(data->loop_thr), SIGUSR1);
	pthread_mutex_unlock(&ctl->lck);
	if (data->loop_thr) pthread_join((*(data->loop_thr)), NULL);
#endif

	free(data->fifo_path);
	free(data);
#ifdef HAVE_PTHREAD
	pthread_mutex_destroy(&ctl->lck);
#endif
}
