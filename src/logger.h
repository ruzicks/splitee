#ifndef	IC_LOGGER_H
#define	IC_LOGGER_H

#include <string.h>

#define	LOGLEVEL_NON 0
#define	LOGLEVEL_ERR 1
#define	LOGLEVEL_INF 2
#define	LOGLEVEL_DBG 3
struct logger;

extern struct logger *logger;


struct logger *
logger_init(FILE *file, int loglevel);

void
logger_free(struct logger *l);

void
log_write(
	struct logger *l,
	int loglevel,
	char *file,
	int line,
	char *fmt,
	...);

void
log_fileline(struct logger *l, char *file, int line);

#define	log(loglevel, ...) log_write(logger, loglevel, \
	__FILE__, __LINE__, __VA_ARGS__)
#define	log_perror(loglevel, msg) log(loglevel, msg ": %s", strerror(errno))

#endif
