/*
 * The core functionality module.
 * Manages reading, writing to file(s), handles controll over fifo ctl
 */
#ifndef	INPUT_CAPTURE_H
#define	INPUT_CAPTURE_H

#include <stdlib.h>
#include <stdio.h>

#include "string_formatter.h"
#include "ic_ctl.h"

#ifndef IC_BUFSIZE
#define	IC_BUFSIZE 4096
#endif

typedef struct {
	struct f_str file_str;	/* string formatter struct	*/
	struct ic_ctl *ctl;	/* for ic_ctl - runtime control	*/
	time_t	last;		/* time of last file creation	*/
	FILE	*capture_file;	/* file to write data into	*/
	int	sigflag_exit;
	int	read_fd;	/* file descriptor - input	*/
	int	write_fd;
	int	split_interval;	/* create new file each N sec.	*/
	char	do_capture;	/* bool value for file capture	*/
} ic_data;

int
ic_init(
    ic_data *ic,
    int read_fd,
    int write_fd,
    char *filename_format,
    char *ctl_path,
    int split_interval);

int
ic_main_loop(ic_data *ic);

void
ic_free(ic_data *ic);

#endif
