DESTDIR=/usr

all:
	$(MAKE) -C src
	mv src/stee bin/

install:
	test -x bin/stee
	mkdir -p "$(DESTDIR)/bin"
	cp bin/* "$(DESTDIR)/bin"

clean:
	$(MAKE) -C src clean
	rm bin/stee

uninstall:
	rm $(DESTDIR)/bin/stee
